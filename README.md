# Russian city

Пакет для выбора города. Содержит фикстуры для стран, регионов и городов.

## Использование

### Коллекции

Коллекция является инстансом класса `Collection` из пакета `core`. По умолчанию доступны методы для поиска `find` и `findOne`.

Из пакета экспортируются коллекции страны, региона, города:
```javascript
export {
  // Страны
  Countries,
  // Регионы
  Regions,
  // Города
  Cities,
};
```

Использование `find` и `findOne`:
```javascript
import { Countries } from 'meteor/russian-city';

// получение списка данных
Countries.finds.find({ selector, options })
  .then(countries => {
    this.state.list = countries;
  })
```

### Компонент

Пакет добавляет тимплейт для последовательного выбора страны -> региона -> города:
```spacebars
{{> SelectCity getCity}}
```

Код хелпера для получения выбранного города:
```javascript
  helpers: {
    getCity() {
      return {
        onChange: (cityId, cityName) => {
          console.log(cityId);
          console.log(cityName);
        },
      };
    },
  }
```

### Параметры компонента
```javascript
  // Получение ид города после выбора
  onChange: { type: Function, optional: true },
  // класс для контейнера компонента
  containerClass: { type: String, defaultValue: '' },
  // класс для контейнера селекта страны
  countryContainerClass: { type: String, defaultValue: 'mb-3' },
  // класс для контейнера селекта региона
  regionContainerClass: { type: String, defaultValue: 'mb-3' },
  // класс для контейнера селекта города
  cityContainerClass: { type: String, defaultValue: 'mb-3' },
  // ид выбраной страны
  selectCountryId: { type: String, optional: true },
  // ид выбраного региона
  selectRegionsId: { type: String, optional: true },
  // ид выбраного города
  selectCityId: { type: String, optional: true },
```

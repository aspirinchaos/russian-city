import { Document, Collection } from 'meteor/core';
import Schema from './schema.js';

const modulename = 'City';

class City extends Document {

}

const Cities = new Collection('Location_City', City, Schema, { customFind: true });

/**
 * Список городов для селекта с учетом страны и региона
 * @param {string} _idCountry - страна
 * @param {string} _idRegion - регион
 * @returns {Promise<Array<{label, value}>>}
 */
Cities.listCities = (_idCountry, _idRegion) => Cities.finds.find({
  selector: {
    _idCountry,
    _idRegion,
  },
  options: {
    sort: {
      sort: 1,
      name: 1,
    },
  },
}).then(cities => cities.map(c => ({ label: c.name, value: c._id })));

export { Cities as default, modulename };

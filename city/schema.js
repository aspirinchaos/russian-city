import SimpleSchema from 'simpl-schema';
import { CoreSchema } from 'meteor/core';

const CountrySchema = new SimpleSchema({
  _id: {
    type: String,
    label: 'id',
  },
  name: {
    type: String,
    label: 'Наименование',
  },
  _idCountry: {
    type: String,
    label: 'Страна',
  },
  _idRegion: {
    type: String,
    label: 'Регион',
  },
  sort: {
    type: Number,
    label: 'Сортировка',
    defaultValue: 0,
  },
  archive: {
    type: Boolean,
    label: 'Удален',
    defaultValue: false,
  },
}).extend(CoreSchema);

export default CountrySchema;

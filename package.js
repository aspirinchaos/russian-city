Package.describe({
  name: 'russian-city',
  version: '0.1.3',
  summary: 'Select city with methods',
  git: 'https://bitbucket.org/aspirinchaos/russian-city.git',
  documentation: 'README.md',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use([
    'ecmascript',
    'tmeasday:check-npm-versions',
    'core',
    'templating',
    'template-controller',
    'select2',
  ]);
  api.addAssets([
    'private/city.json',
  ], 'server');
  api.addFiles('server/fixtures.js', 'server');
  api.addFiles([
    'client/select-city.js',
    'client/select-only-country.js',
    'client/select-only-region.js',
    'client/select-only-city.js',
  ], 'client');
  api.mainModule('russian-city.js');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('russian-city');
  api.mainModule('russian-city-tests.js');
});

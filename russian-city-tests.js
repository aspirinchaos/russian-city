// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by russian-city.js.
import { name as packageName } from "meteor/russian-city";

// Write your tests here!
// Here is an example.
Tinytest.add('russian-city - example', function (test) {
  test.equal(packageName, "russian-city");
});

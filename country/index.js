import { Document, Collection } from 'meteor/core';
import Schema from './schema.js';

const modulename = 'Country';

class Country extends Document {

}

const Countries = new Collection('Location_Country', Country, Schema, { customFind: true });

/**
 * Список стран для селекта
 * @returns {Promise<Array<{label, value}>>}
 */
Countries.listCountries = () => Countries.finds.find({
  options: {
    sort: {
      sort: 1,
      name: 1,
    },
  },
}).then(countries => countries.map(c => ({ label: c.name, value: c._id })));

export { Countries as default, modulename };

import SimpleSchema from 'simpl-schema';
import { CoreSchema } from 'meteor/core';

const CountrySchema = new SimpleSchema({
  name: {
    type: String,
    label: 'Наименование',
  },
  name_en: {
    type: String,
    label: 'Наименование en',
    optional: true,
  },
  sort: {
    type: Number,
    label: 'Сортировка',
    defaultValue: 0,
  },
  archive: {
    type: Boolean,
    label: 'Удален',
    defaultValue: false,
  },
}).extend(CoreSchema);

export default CountrySchema;

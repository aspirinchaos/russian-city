import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import Countries from '../country';
import Regions from '../region';
import Cities from '../city';
// import template
import './select-city.html';

TemplateController('SelectCity', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    onChange: { type: Function, optional: true },
    containerClass: { type: String, defaultValue: '' },
    countryContainerClass: { type: String, defaultValue: 'mb-3' },
    regionContainerClass: { type: String, defaultValue: 'mb-3' },
    cityContainerClass: { type: String, defaultValue: 'mb-3' },
    _idCountry: { type: String, optional: true },
    _idRegion: { type: String, optional: true },
    _idCity: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {
    countries: [],
    regions: [],
    cities: [],
    country: '',
    region: '',
    city: '',
  },

  // Lifecycle callbacks work exactly like with standard Blaze
  async onCreated() {
    this.state.country = this.props._idCountry || '';
    this.state.region = this.props._idRegion || '';
    this.state.city = this.props._idCity || '';
    this.state.countries = await Countries.listCountries();
    this.autorun(async () => {
      if (this.state.country) {
        this.state.regions = await Regions.listRegions(this.state.country);
      }
    });
    this.autorun(async () => {
      if (this.state.region) {
        this.state.cities = await Cities.listCities(this.state.country, this.state.region);
      }
    });
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    countries() {
      return {
        options: this.state.countries,
        placeholder: 'Выберите страну',
        onChange: (value) => {
          this.state.country = value;
        },
        className: 'form-control',
        selected: this.state.country,
      };
    },
    regions() {
      return {
        options: this.state.regions,
        placeholder: 'Выберите регион',
        onChange: (value) => {
          this.state.region = value;
        },
        className: 'mt-3',
        selected: this.state.region,
      };
    },
    cities() {
      return {
        options: this.state.cities,
        placeholder: 'Выберите город',
        onChange: (city, name) => {
          const { country, region } = this.state;
          this.props.onChange(city, name, country, region);
        },
        className: 'mt-3',
        selected: this.state.city,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});

import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './select-only-country.html';
import Countries from '../country';

TemplateController('SelectOnlyCountry', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    params: { type: Object, blackbox: true, defaultValue: {} },
  }),

  // Setup private reactive template state
  state: {
    countries: [],
  },

  // Lifecycle callbacks work exactly like with standard Blaze
  async onCreated() {
    this.state.countries = await Countries.listCountries();
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    countries() {
      return {
        options: this.state.countries,
        className: 'form-control',
        ...this.props.params,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});

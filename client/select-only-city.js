import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './select-only-city.html';
import Cities from '../city';

TemplateController('SelectOnlyCity', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    country: { type: String, optional: true },
    region: { type: String, optional: true },
    params: { type: Object, blackbox: true, defaultValue: {} },
  }),

  // Setup private reactive template state
  state: {
    cities: [],
  },

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    this.autorun(async () => {
      const { region, country } = this.props;
      if (region && country) {
        this.state.cities = await Cities.listCities(country, region);
      }
    });
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    cities() {
      return {
        options: this.state.cities,
        className: 'form-control',
        ...this.props.params,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});

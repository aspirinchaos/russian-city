import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './select-only-region.html';
import Regions from '../region';

TemplateController('SelectOnlyRegion', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    country: { type: String, optional: true },
    params: { type: Object, blackbox: true, defaultValue: {} },
  }),

  // Setup private reactive template state
  state: {
    regions: [],
  },

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    this.autorun(async () => {
      if (this.props.country) {
        this.state.regions = await Regions.listRegions(this.props.country);
      }
    });
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    regions() {
      return {
        options: this.state.regions,
        className: 'form-control',
        ...this.props.params,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});

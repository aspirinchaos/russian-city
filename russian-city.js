import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
import Countries from './country';
import Regions from './region';
import Cities from './city';

checkNpmVersions({
  'simpl-schema': '1.5.3',
}, 'russian-city');

export {
  Countries,
  Regions,
  Cities,
};

import { Meteor } from 'meteor/meteor';
import Countries from '../country';
import Regions from '../region';
import Cities from '../city';

Meteor.startup(() => {
  if (!Countries.mongo.find().count()) {
    const json = JSON.parse(Assets.getText('private/city.json'));
    json.forEach(({ name: cn, name_en, regions }) => {
      const _idCountry = Countries.insert({ name: cn, name_en });
      regions.forEach(({ name: rn, cities }) => {
        const _idRegion = Regions.insert({ name: rn, _idCountry });
        cities.forEach(({ name }) => {
          Cities.insert({ name, _idCountry, _idRegion });
        });
      });
    });
    console.log('Все города добавлены');
  }
});

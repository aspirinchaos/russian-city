import { Document, Collection } from 'meteor/core';
import Schema from './schema.js';

const modulename = 'Region';

class Region extends Document {

}

const Regions = new Collection('Location_Region', Region, Schema, { customFind: true });

/**
 * Список регионов для селекта с учетом страны
 * @param {string} _idCountry - страна
 * @returns {Promise<Array<{label, value}>>}
 */
Regions.listRegions = _idCountry => Regions.finds.find({
  selector: {
    _idCountry,
  },
  options: {
    sort: {
      sort: 1,
      name: 1,
    },
  },
}).then(regions => regions.map(r => ({ label: r.name, value: r._id })));

export { Regions as default, modulename };
